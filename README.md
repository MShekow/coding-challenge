# coding-challenge

## Approach

- Read the coding challenge, asked for feedback via email
- Choose high-level approach: **API-driven development**, where I specify the API as OpenAPI 3 format, from which server
  stubs and client stubs (and entity objects) are generated. This reduces the time I would need to spend on writing
  boilerplate code, and it improves type-safety
- Technology prep: spend a few hours (on very simple demo projects that are unrelated to this coding challenge) to learn
  about Java, Kotlin, Spring (Boot) and concrete technologies for API-to-code generation. Choose JVM-based languages
  over Go because they are in higher demand in the companies I'm interested in. Choose Java+Maven because I have
  slightly more knowledge in it than Kotlin and Gradle, although I'd prefer them, given more time and familiarity.
- Create ER model to get a better understanding of the underlying database that would be used (not actually implemented)
  . I created the ER model using PlantUML, which is a fantastic Documentation-as-Code tool which I've blogged
  about [here](https://www.augmentedmind.de/2021/01/03/plantuml-tutorial-diagrams-as-code/). While creating the ER
  model, I also created the Glossary. See [`backend/er-model.puml`](backend/er-model.puml) file for the ER model.
- Design the API, using [Apicurio studio](https://studio.apicur.io). Because REST is not really a fully specified
  standard, I refer to the [Zalando](https://opensource.zalando.com/restful-api-guidelines)
  and [Microsoft](https://github.com/microsoft/api-guidelines/blob/vNext/Guidelines.md) REST guidelines.
  See [here](backend/src/main/resources/static/training-api.yaml) for the spec file.
- Design Git repository structure: use monorepo approach which is suitable for this kind of monolithic application,
  putting the frontend and backend in separate subfolders
- Create a Spring Boot application using the Spring Initializr, modify it to generate the interface class files from the
  OpenAPI spec, implement an in-memory data store of the data objects, then implement unit tests for a few of the APIs
- Create an Angular application, generate the HTTP client from the OpenAPI spec, build a very simple UI that performs
  the following steps (values being hard-coded):
    - Display list of trainings
    - Create a training (which should update the list)

## Glossary

| German            | English     |
|-------------------|-------------|
| Schulung          | training    |
| Dozent            | instructor  |
| Termin (Schulung) | date        |
| Kunde/Teilnehmer  | participant |
| Buchung           | booking     |

## Testing concept

Normally, a thorough testing concept should be created, together with the customer (also to approve the testing budget).
In this coding challenge, I'm limiting tests to functional unit tests on the API layer, but in practice there should be
many more kinds of tests, such as non-functional test types (e.g. installation tests or performance tests), or
functional tests (e.g. end-to-end tests or acceptance tests).

When limiting ourselves to functional unit tests for the API, there are many strategies that help to find
bugs effectively, which I have not applied due to time constraints. These strategies include black-box testing (e.g.
Boundary-Value testing, Equivalence Class testing, or property-based testing) and white-box testing (e.g. decision-tree
testing or mutation testing).

## Security considerations

See [here](security-considerations.md)

## DevOps considerations

See [here](devops.md)

## Future / incomplete work

Many things were not done due to time constraints:

- Not all backend features are implemented
- READMEs are missing that are specific to the `frontend` and `backend` projects (they would e.g. explain how to set up
  the project or how to run tests)
- More testing is needed (not all functionality is tested, and other types of tests should also be done)
- API is not versioned
- API does not offer pagination, sorting, or advanced filtering (e.g. only listing trainings that still have free slots)
- No security-related measures are implemented
- No persistence of data
- The OpenAPI model is not fully complete: e.g. examples or descriptions are missing (or kept very brief)
- No error handling in the frontend

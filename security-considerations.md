# Security considerations

On a conceptual basis, I would consider the following security-related aspects.

[[_TOC_]]

## API usage level

The API/backend needs to be protected from unauthorized intrusions:

- Add user authentication & authorization (e.g. with RBAC). The system needs user accounts, authentication schemes (e.g.
  OAuth 2.0 or OIDC). Domain objects (e.g. a training) must belong to specific users, and roles and permissions need to
  be defined (and checked at run-time). Two-Factor-Auth should also be considered.
- Don't implement such things yourself but use professional frameworks for things like account management (and
  password/token encryption/validation), handling of sessions/tokens, RBAC-validations, XSS-prevention mechanisms (CSRF)
  , or input handling (e.g. to prevent SQL-injection attacks)

## Software-development level

Make use of SAST (Static Application Security Testing) tools in a CI/CD pipeline. These are static code scanner which
scan our source code for potential weaknesses, errors and bugs, such as SQL-injection attacks, buffer overflows, etc.
There is a plethora of tools, and they should cover both code _and_ configuration files (e.g. IaC, `Dockerfile`, etc.).

Tool examples: SonarQube, Findbugs/Spotbugs, PMD, Fortify, Kics, checkov, Sonatype Lift, ...

## Supply chain security

Often security bugs are not in our own code, but in the dependencies we use. SCA (Software Composition Analysis) tools
analyze your dependencies and notify you about outdated and/or insecure ones, e.g. automatically creating PRs that bump
the dependency version. These tools should also be run in CI/CD, not just on "Git push" but also in _regular
intervals_ (once the software is in maintenance mode where no one develops features for it anymore).

A plethora of tools exist: SonarQube, Renovate Bot, dependabot, Trivy, ...

In high stake applications (banking etc. - not applicable for this demo app), you would also need to verify the sources
of (seemingly secure) packages: you have to verify signatures of packages (such as Docker containers). See e.g.
the [sigstore project](https://github.com/sigstore) for a possible implementation.

## Deployment security

While deploying or operating the software, many security-related issues can occur:

- Docker images may already be insecure at _build time_, e.g. when using the `root` user. I blogged about possible
  mitigations in [this article](https://www.augmentedmind.de/2022/02/20/optimize-docker-image-security/)
- You must ensure that the runtime environment (e.g. Kubernetes cluster, application server, etc.) is up-to-date _and
  secure_ (can e.g. be achieved by using Docker images for deployment, and SCA tools like dependabot that ensure that
  versions are up-to-date)
- You need mechanisms that detect and protect you from intrusions. You need to cover both the "edge" / API of your
  system (e.g. Denial of Service attacks), and the _internal_ parts of your system (e.g. detecting unknown Kubernetes
  pods or unexpected pod activity, incorrect configurations). There are hundreds of tools (free and commercial), e.g.
  Aquasecurity, Neuvector, Sysdig, OPA Gatekeeper, ...
- We can consider DAST (Dynamic Application Security Testing), where attacks are simulated on a running instance of the
  software, e.g. with [ZAP](https://www.zaproxy.org/) or Fortify
- Make use of HTTPS only to access the service, to make sniffing of credentials impossible (MITM attacks). Ensure that
  the SSL-related server configuration is disabling insecure protocols (e.g. TLS <= 1.1) and ciphers (see
  e.g. [here](https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html))

## Get help from professionals

Developers (or ops) are not security experts. Therefore, for high-stake applications, the team should get help from
security professionals (consultants) who know how to do a proper threat analysis, and how to mitigate threats they find
in a security audit.

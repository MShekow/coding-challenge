#!/bin/bash

# Cleans the oudated content that the volume may have
rm -rf /app_docker/node_modules/*
# Copies over the node_modules that are baked into the image (its build stage)
cp -a /app/node_modules/. /app_docker/node_modules/

# Copy remaining files (excluding the "src" folder, because "src" is bind-mounted from the host)
cp /app/*.json /app_docker/

# Start the development server
# Note: in the container we need to make the NG dev server reachable from all interfaces, to be able to access it
# from the host
cd /app_docker
npm run start -- --host=0.0.0.0

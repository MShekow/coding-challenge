import {Component} from '@angular/core';
import {DefaultService, Training} from "../core/api/v1";
import {BehaviorSubject, switchMap} from "rxjs";

@Component({
  selector: 'app-training-demo',
  templateUrl: './training-demo.component.html',
  styleUrls: ['./training-demo.component.scss']
})
export class TrainingDemoComponent {

  // Apply trick from https://blog.eyas.sh/2018/12/data-and-page-content-refresh-patterns-in-angular/
  // to have an easy-to-use way to trigger refreshes of trainings$
  private readonly refreshToken$ = new BehaviorSubject(undefined);
  trainings$ = this.refreshToken$.pipe(
    switchMap(() => this.webService.listTrainings())
  );

  constructor(private webService: DefaultService) {
  }


  createTraining() {
    const generateRandomString = () => Math.random().toString(36).slice(2, 7);
    const training: Training = {
      description: `Random description: ${generateRandomString()}`,
      instructorName: `Instructor: ${generateRandomString()}`,
      priceEuro: Math.floor(Math.random() * 3000) + 1 // price between 1 and 3001 Euros
    }

    this.webService.createTraining(training).subscribe((training_) => {
      this.refreshToken$.next(undefined); // Trigger a refresh of trainings$
    });
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingDemoComponent } from './training-demo.component';

describe('TrainingDemoComponent', () => {
  let component: TrainingDemoComponent;
  let fixture: ComponentFixture<TrainingDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

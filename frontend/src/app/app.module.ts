import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {TrainingDemoComponent} from './training-demo/training-demo.component';
import {ApiModule, Configuration, ConfigurationParameters,} from './core/api/v1';
import {environment} from "../environments/environment";
import {HttpClientModule} from "@angular/common/http";

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    basePath: environment.backendPath,
  };
  return new Configuration(params);
}

@NgModule({
  declarations: [
    TrainingDemoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ApiModule.forRoot(apiConfigFactory),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [TrainingDemoComponent]
})
export class AppModule {
}

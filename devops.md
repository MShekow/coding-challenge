# DevOps considerations

On a conceptual basis, I would consider the following DevOps-related aspects.

[[_TOC_]]

## Application deployment

In general, the frontend and backend module of the application should each be deployed as (Docker / OCI) containers.

- For the `backend`, I would choose one of the options explained in
  the [official guide](https://spring.io/guides/topicals/spring-boot-docker/). For instance, just
  calling `./mvnw spring-boot:build-image -Dspring-boot.build-image.imageName=myorg/myapp` is a very convenient way to
  build a Docker image, but I have not yet analyzed in detail how "good" it is (e.g. regarding size or security),
  compared to hand-crafting the `Dockerfile`.
- For the `frontend`, I would use Docker's **multi-stage** builds to build the Angular app in a `node` container, and
  then copy the `dist` folder into a secure [nginx-unprivileged](https://hub.docker.com/r/nginxinc/nginx-unprivileged)
  image. Depending on where the frontend image is deployed (e.g. Docker-compose vs. Kubernetes), the `nginx.conf` file
  must be different:
    - When building for Kubernetes or similar platforms, there already are other mechanisms (e.g. the _ingress_ for
      Kubernetes) that take care of aspects such as API reverse proxying, SSL termination, load balancing, DoS attacks,
      etc. - therefore, the `nginx.conf` can be kept minimal, and only serve static files
    - In other environments (e.g. for Docker) the `nginx.conf` file can take care of the aspects mentioned above (SSL
      termination, etc.). Alternatively, we need to define yet another service (the reverse-proxy) that takes care of
      these responsibilities

Depending on the expected application **load**, I would choose the environment as follows:

- Little load (a few dozen users using the page in parallel): a single-host **docker-compose** setup is sufficient,
  for instance using a `docker-compose.yml` file which defines services for the database, frontend and backend
- If load is higher, there are several options, such as packaging the application as a Helm chart and installing it into
  a (managed) Kubernetes cluster (e.g. Azure AKS), or a scalable container services (like Azure Container Instances).
  Either way, I would recommend to then use a _managed_ database offering, because running scalable DBs ourselves is
  very tricky.

The basic idea is: keep the complexity as low as possible, as this reduces costs of building and operating the system.

## Observability

The key to having smooth operations (where you _detect_ errors quickly, and can also _recover_ from them quickly once
you know about the error) is observability. I would make use of existing tooling to collect metrics and logs of
infrastructure, services (e.g. database server, caches, reverse proxies, ...), and my team's code (on frontend and
backend side). This includes extending our code to offer app-specific metrics (e.g. the cancellation rate of bookings).
Then, I would set up carefully-selected alerts (as explained in
my [blog post](https://www.augmentedmind.de/2021/10/31/alerting-best-practices/)) so that we _detect_ errors quickly.
Quick _recovery_ of errors is aided by the fact that we have already collected and aggregated metrics and logs, and can
use analysis tools (such as Grafana) to do a root cause analysis.

## Automation (CI/CD)

As I explain in detail in [this blog post](https://www.augmentedmind.de/2021/08/22/ci-cd-basics/), CI/CD helps reduce
costs with the help of automation. In the post I also describe in detail what kinds of typical tasks there are for each
stage (Cont. Integration, Cont. Delivery, Cont. Deployment).

The tricky bit is to be smart about the choice where to start, and explain to the customer the benefits of doing this
"front-loading" of efforts. This can e.g. be "the software will be much easier to maintain long-term when using
code analysis tools", or "you can check the preview environment daily and give feedback on new features as we develop
them, reducing waste".

Steps I would do for _this_ project (in roughly this order), using a CI/CD system that we internally (and together with
the customer) agree on:

- Basic tests & builds:
    - Run automated tests on each push
    - Create automatic builds (e.g. of Docker containers that are pushed to a registry)
- Basic security:
    - Add SCA (automatic dependency updates), e.g. with dependabot, Renovatebot, etc. - requires appointing one or more
      developers who are in charge of merging the generated PRs
    - Scan code using various SAST-tools such as Fortify, SonarQube... (see _Supply chain security_
      in [security considerations](security-considerations.md))
    - Scan deployment artefacts (such as Docker images) for vulnerabilities (with Trivy, etc.)
- Basic Cont. Deployment: create a (one or more) _preview environments_ to show the latest features to the customer/team
- Establish common coding standards e.g. using linters (adapting an existing code base can be a lot of work)
- Build and run more elaborate tests (beyond Unit tests), e.g. using the containers. E.g. load tests, smoke tests, e2e)
    - Tweak SCA tools to allow for more _fully automated_ merges of dependency update PRs, given that there are now
      tests that can detect problems caused by the dependency update

Finally, it is important to note that the decision of what to automate needs to be discussed within the **team** as
well. Just introducing some tools is not enough. For instance, if your team does not have a test culture, it is of
little help if the "DevOps guy/gal" implemented automatic test execution, when no one actually creates (new) tests.

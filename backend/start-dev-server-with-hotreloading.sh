#!/bin/bash

# Start a continuous build (which detects changes in .java files and compiles them to .class files) in the background
# and silence its output
./gradlew build --continuous >/dev/null &

# Start the Spring Boot development server, whose hot-reloading is only triggered on .class file changes
./gradlew bootRun

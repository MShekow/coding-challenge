package com.example.training;

import com.example.training.model.Training;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TrainingApiTests {
    @Autowired
    TestRestTemplate restTemplate;

    /**
     * Note: in a real project, I would make a test plan and create more fine-grained tests, that each only test one
     * aspect at a time. This speeds up finding errors once suddenly tests start failing in CI: just having a few
     * "heavy" test cases (like the one below, which tests both creation of multiple objects and that a GET requests
     * returns them correctly) is problematic, because the error analysis is more difficult.
     * Example for a test that only does one thing: make the POST request to "/trainings", and mock the
     * TrainingInMemoryRepository class; the test verifies that the mocked repository contains the POSTed Training
     * object with a pre-assigned ID.
     *
     * There is nothing wrong with having tests that test multiple things - as long as we ALSO have the "atomic" tests
     * that test only one thing at a time.
     */

    /**
     * Creates two Trainings and expects that their ID (generated on the server-side) is different. Expects that the
     * total number of returned Trainings is two when making a GET request to /trainings.
     */
    @Test
    void testCreateTwoTrainingsSuccessfulWithDifferentIds() {
        Training training1 = new Training();
        training1.setDescription("Training 1 Description");
        training1.setInstructorName("Instructor");
        training1.setPriceEuro(1000);
        ResponseEntity<Training> returnedTrainingResponse1 = restTemplate.postForEntity("/trainings", training1, Training.class);
        assertThat(returnedTrainingResponse1.hasBody()).isTrue();

        Training training2 = new Training();
        training2.setDescription("Training 2 Description");
        training2.setInstructorName("Instructor");
        training2.setPriceEuro(1100);
        ResponseEntity<Training> returnedTrainingResponse2 = restTemplate.postForEntity("/trainings", training2, Training.class);

        assertThat(returnedTrainingResponse2.hasBody()).isTrue();

        assertThat(returnedTrainingResponse1.getBody().getId()).isNotEqualTo(returnedTrainingResponse2.getBody().getId());

        Training[] trainingList = restTemplate.getForObject("/trainings", Training[].class);
        assertThat(trainingList.length).isEqualTo(2);
    }
}

package com.example.training.api;

import com.example.training.TrainingsApi;
import com.example.training.model.Training;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.OffsetDateTime;
import java.util.List;

@RestController
public class TrainingApiController implements TrainingsApi {

    private final TrainingInMemoryRepository repository;

    public TrainingApiController(TrainingInMemoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public ResponseEntity<Training> createTraining(Training training) {
        // TODO: right now, training.get...() may return null!!!!
        //  -> figure out how to convince Spring to reject requests (code 400) that are missing required fields
        //  Note: the same problem applies to all other methods!
        if (training.getPriceEuro() <= 0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Price must be positive");
        if (training.getDescription().equals("") || training.getInstructorName().equals(""))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Instructor name and description may not be " +
                    "empty");

        return new ResponseEntity<>(repository.storeTraining(training), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Training>> listTrainings(OffsetDateTime start, OffsetDateTime end) {
        System.out.println("test5");
        return new ResponseEntity<>(repository.listTrainings(start, end), HttpStatus.OK);
    }
}

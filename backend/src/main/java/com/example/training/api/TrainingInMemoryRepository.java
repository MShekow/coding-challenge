package com.example.training.api;

import com.example.training.model.Training;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple fake / in-memory data store for Training objects
 */
@Service
public class TrainingInMemoryRepository {
    private final Map<Integer, Training> trainings = new HashMap<>();
    private int trainingId = 0;
    private int trainingDateId = 0;

    public Training storeTraining(Training training) {
        Training trainingCopy = new Training();
        trainingCopy.setId(trainingId++);
        trainingCopy.setDescription(training.getDescription());
        trainingCopy.setInstructorName(training.getInstructorName());
        trainingCopy.setPriceEuro(training.getPriceEuro());
        trainings.put(trainingCopy.getId(), trainingCopy);
        return trainingCopy;
    }

    public Training getTraining(Integer id) throws NullPointerException {
        Training training = trainings.get(id);
        if (training == null) throw new NullPointerException();
        return training;
    }

    public List<Training> listTrainings(OffsetDateTime start, OffsetDateTime end) {
        // TODO actually implement filtering
        return new ArrayList<>(this.trainings.values());
    }
}

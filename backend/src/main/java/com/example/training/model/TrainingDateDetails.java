package com.example.training.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.format.annotation.DateTimeFormat;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Provides details about an individual day that belongs to a TrainingDate
 */

@Schema(name = "TrainingDateDetails", description = "Provides details about an individual day that belongs to a TrainingDate")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-07T18:00:43.792358+02:00[Europe/Berlin]")
public class TrainingDateDetails   {

  @JsonProperty("location")
  private String location;

  @JsonProperty("start")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime start;

  @JsonProperty("end")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime end;

  public TrainingDateDetails location(String location) {
    this.location = location;
    return this;
  }

  /**
   * The description of the location of a location (e.g. a street address, or that it is a virtual event)
   * @return location
  */
  @NotNull 
  @Schema(name = "location", description = "The description of the location of a location (e.g. a street address, or that it is a virtual event)", required = true)
  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public TrainingDateDetails start(OffsetDateTime start) {
    this.start = start;
    return this;
  }

  /**
   * Get start
   * @return start
  */
  @NotNull @Valid 
  @Schema(name = "start", required = true)
  public OffsetDateTime getStart() {
    return start;
  }

  public void setStart(OffsetDateTime start) {
    this.start = start;
  }

  public TrainingDateDetails end(OffsetDateTime end) {
    this.end = end;
    return this;
  }

  /**
   * Get end
   * @return end
  */
  @NotNull @Valid 
  @Schema(name = "end", required = true)
  public OffsetDateTime getEnd() {
    return end;
  }

  public void setEnd(OffsetDateTime end) {
    this.end = end;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrainingDateDetails trainingDateDetails = (TrainingDateDetails) o;
    return Objects.equals(this.location, trainingDateDetails.location) &&
        Objects.equals(this.start, trainingDateDetails.start) &&
        Objects.equals(this.end, trainingDateDetails.end);
  }

  @Override
  public int hashCode() {
    return Objects.hash(location, start, end);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TrainingDateDetails {\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    start: ").append(toIndentedString(start)).append("\n");
    sb.append("    end: ").append(toIndentedString(end)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


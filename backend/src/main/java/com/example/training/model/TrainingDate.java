package com.example.training.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * TrainingDate
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-07T18:00:43.792358+02:00[Europe/Berlin]")
public class TrainingDate   {

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("availableSeats")
  private Integer availableSeats;

  @JsonProperty("details")
  @Valid
  private List<TrainingDateDetails> details = new ArrayList<>();

  public TrainingDate id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @NotNull 
  @Schema(name = "id", required = true)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public TrainingDate availableSeats(Integer availableSeats) {
    this.availableSeats = availableSeats;
    return this;
  }

  /**
   * The number of free seats (or: how many bookings can still be made)
   * @return availableSeats
  */
  
  @Schema(name = "availableSeats", description = "The number of free seats (or: how many bookings can still be made)", required = false)
  public Integer getAvailableSeats() {
    return availableSeats;
  }

  public void setAvailableSeats(Integer availableSeats) {
    this.availableSeats = availableSeats;
  }

  public TrainingDate details(List<TrainingDateDetails> details) {
    this.details = details;
    return this;
  }

  public TrainingDate addDetailsItem(TrainingDateDetails detailsItem) {
    this.details.add(detailsItem);
    return this;
  }

  /**
   * Get details
   * @return details
  */
  @NotNull @Valid 
  @Schema(name = "details", required = true)
  public List<TrainingDateDetails> getDetails() {
    return details;
  }

  public void setDetails(List<TrainingDateDetails> details) {
    this.details = details;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrainingDate trainingDate = (TrainingDate) o;
    return Objects.equals(this.id, trainingDate.id) &&
        Objects.equals(this.availableSeats, trainingDate.availableSeats) &&
        Objects.equals(this.details, trainingDate.details);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, availableSeats, details);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TrainingDate {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    availableSeats: ").append(toIndentedString(availableSeats)).append("\n");
    sb.append("    details: ").append(toIndentedString(details)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


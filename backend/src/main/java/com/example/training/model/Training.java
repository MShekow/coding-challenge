package com.example.training.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Training
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-07T18:00:43.792358+02:00[Europe/Berlin]")
public class Training   {

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("description")
  private String description;

  @JsonProperty("instructorName")
  private String instructorName;

  @JsonProperty("priceEuro")
  private Integer priceEuro;

  public Training id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  
  @Schema(name = "id", required = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Training description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  */
  @NotNull 
  @Schema(name = "description", required = true)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Training instructorName(String instructorName) {
    this.instructorName = instructorName;
    return this;
  }

  /**
   * Get instructorName
   * @return instructorName
  */
  @NotNull 
  @Schema(name = "instructorName", required = true)
  public String getInstructorName() {
    return instructorName;
  }

  public void setInstructorName(String instructorName) {
    this.instructorName = instructorName;
  }

  public Training priceEuro(Integer priceEuro) {
    this.priceEuro = priceEuro;
    return this;
  }

  /**
   * Get priceEuro
   * @return priceEuro
  */
  @NotNull 
  @Schema(name = "priceEuro", required = true)
  public Integer getPriceEuro() {
    return priceEuro;
  }

  public void setPriceEuro(Integer priceEuro) {
    this.priceEuro = priceEuro;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Training training = (Training) o;
    return Objects.equals(this.id, training.id) &&
        Objects.equals(this.description, training.description) &&
        Objects.equals(this.instructorName, training.instructorName) &&
        Objects.equals(this.priceEuro, training.priceEuro);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, description, instructorName, priceEuro);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Training {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    instructorName: ").append(toIndentedString(instructorName)).append("\n");
    sb.append("    priceEuro: ").append(toIndentedString(priceEuro)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

